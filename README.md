# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen in (default: `8000`). This is the port that NGINX server listens on.
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to froward requests to (default: `9000`)
